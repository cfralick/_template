# A set of useful git hooks

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/cfralick/template?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

These hooks automate some tasks with the help of git.
For the moment, it is mostly hooks related to tools from the php ecosystem.

## Documentation

Read [the documentation][1] to learn more about git template.

## Contributing

see [CONTRIBUTING.md][2]

## Credits

Inspired by [Tim Pope][3]

[1]: http://git-template.readthedocs.org
[2]: ./CONTRIBUTING.md
[3]: http://tbaggery.com/2011/08/08/effortless-ctags-with-git.html
