#!/bin/bash

$( command -v  jsonlint ) > /dev/null 2>&1
if [ $? -eq 0 ]; then
    jsonlint $FILE > /dev/null
else 
    $( command -v python ) > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        python -mjson.tool $FILE > /dev/null
    fi
fi
